import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ionic-multi-step-form-demo',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;

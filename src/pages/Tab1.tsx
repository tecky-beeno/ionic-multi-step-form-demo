import {
  IonButton,
  IonContent,
  IonFooter,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonPage,
  IonProgressBar,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import { useState } from 'react'
import './Tab1.css'

type FormState = {
  age: number | null
  name: string
  color: string
}

const Tab1: React.FC = () => {
  const [state, setState] = useState<FormState>({
    age: null,
    name: '',
    color: '',
  })
  function patchState<K extends keyof FormState>(
    field: K,
    value: FormState[K],
  ) {
    setState({ ...state, [field]: value })
  }
  const [step, setStep] = useState(1)
  const MaxStep = 3
  function nextStep() {
    if (step < MaxStep) {
      setStep(step + 1)
    }
  }
  function prevStep() {
    if (step > 1) {
      setStep(step - 1)
    }
  }
  const steps: Record<number, JSX.Element> = {
    1: (
      <IonItem>
        <IonLabel>What is your age?</IonLabel>
        <IonInput
          type="number"
          value={state.age}
          onIonChange={e => patchState('age', +e.detail.value! || null)}
        ></IonInput>
      </IonItem>
    ),
    2: (
      <>
        <IonItem>
          <IonLabel>What is your name?</IonLabel>
          <IonInput
            type="text"
            value={state.name}
            onIonChange={e => patchState('name', e.detail.value || '')}
          ></IonInput>
        </IonItem>
        <IonItem>
          <IonLabel>What is your name?</IonLabel>
          <IonInput
            type="text"
            value={state.name}
            onIonChange={e => patchState('name', e.detail.value || '')}
          ></IonInput>
        </IonItem>
        <IonItem>
          <IonLabel>What is your name?</IonLabel>
          <IonInput
            type="text"
            value={state.name}
            onIonChange={e => patchState('name', e.detail.value || '')}
          ></IonInput>
        </IonItem>
      </>
    ),
    3: (
      <IonItem>
        <IonLabel>What is the color?</IonLabel>
        <IonInput
          type="text"
          value={state.color}
          onIonChange={e => patchState('color', e.detail.value || '')}
        ></IonInput>
      </IonItem>
    ),
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 1</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonProgressBar value={step / MaxStep}></IonProgressBar>
        <pre>
          <code>{JSON.stringify({ state, step }, null, 2)}</code>
        </pre>
        {steps[step]}
      </IonContent>
      <IonFooter className="ion-padding">
        <div className="ion-text-center d-flex">
          <IonButton className="grow ion-margin" onClick={prevStep}>
            Prev
          </IonButton>
          <IonButton className="grow ion-margin" onClick={nextStep}>
            Next
          </IonButton>
        </div>
      </IonFooter>
    </IonPage>
  )
}

export default Tab1
